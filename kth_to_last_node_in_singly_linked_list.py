﻿''' Write a function kth_to_last_node() that takes an integer kk and the head_node
    of a singly linked list, and returns the kkth to last node in the list.
'''


class LinkedListNode:

    def __init__(self, value):
        self.value = value
        self.next = None


def kth_to_last_node(k, head):
    '''
    '''
    index = 0
    current = head
    indexer = {index: current.value}
    while current.next:
        index += 1
        current = current.next
        indexer[index] = current.value

    index_of_node_from_head = index - k + 1
    return indexer[index_of_node_from_head]


def kth_to_last_node2(k, head):
    length = 1
    current = head
    while current.next:
        current = current.next
        length += 1

    index_of_node_from_head = length - k

    current = head
    for i in xrange(index_of_node_from_head):
        current = current.next

    return current.value


a = LinkedListNode("Angel Food")
b = LinkedListNode("Bundt")
c = LinkedListNode("Cheese")
d = LinkedListNode("Devil's Food")
e = LinkedListNode("Eccles")

a.next = b
b.next = c
c.next = d
d.next = e

# returns the node with value "Devil's Food" (the 2nd to last node)
print kth_to_last_node(2, a)
print kth_to_last_node2(2, a)
