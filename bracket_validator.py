﻿''' Write an efficient function that tells us whether or not an input
    string's openers and closers are properly nested.
'''


def validate(string):
    '''
    '''
    s = []
    pos = 0

    try:
        while pos < len(string):
            char = string[pos]

            if char in ['(', '{', '[']:
                s.append(char)

            if char == ')':
                if s[-1] == '(':
                    s.pop()
                else:
                    break

            if char == ']':
                if s[-1] == '[':
                    s.pop()
                else:
                    break

            if char == '}':
                if s[-1] == '{':
                    s.pop()
                else:
                    break

            pos += 1
        return not s
    except IndexError:
        return False


test1 = "{ [ ] ( ) }"
test2 = "{ [ ( ] ) }"
test3 = "{ [ }"
test4 = 'a*{b*[c+d]-(x+y)}'
test5 = '}[]'

tests = [test1, test2, test3, test4, test5]

for i in xrange(100000):
    for t in tests:
        validate(t)
