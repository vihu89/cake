﻿''' Write a function reverse_words() that takes a string message and reverses the
    order of the words in-place.
'''


def reverse_words(words):
    '''
    '''
    string_list = words.split()

    left_pointer = 0
    right_pointer = len(string_list) - 1

    while left_pointer < right_pointer:

        # swap characters
        string_list[left_pointer], string_list[right_pointer] = \
            string_list[right_pointer], string_list[left_pointer]

        # move towards middle
        left_pointer += 1
        right_pointer -= 1

    return ' '.join(string_list)


message = 'find you will pain only go you recordings security the into if'
print message
# returns: 'if into the security recordings you go only pain will you find'
print reverse_words(message)
