﻿''' Write a class TempTracker with these methods:

    insert()—records a new temperature
    get_max()—returns the highest temp we've seen so far
    get_min()—returns the lowest temp we've seen so far
    get_mean()—returns the mean ↴ of all temps we've seen so far
    get_mode()—returns the mode ↴ of all temps we've seen so far

    Optimize for space and time. Favor speeding up the getter functions
    (get_max(), get_min(), get_mean(), and get_mode()) over speeding up the insert() function.

    get_mean() should return a float, but the rest of the getter functions can return integers.
    Temperatures will all be inserted as integers. We'll record our temperatures in Fahrenheit,
    so we can assume they'll all be in the range 0..110

    If there is more than one mode, return any of the modes.
'''
from collections import Counter


class TempTracker(object):
    def __init__(self):
        self.min_temp = None
        self.max_temp = None
        self.total_temps = 0
        self.total_sum = 0
        self.mean = None
        self.freq = Counter()

    def insert(self, temp):
        if (self.max_temp is None) or (temp > self.max_temp):
            self.max_temp = temp
        if (self.min_temp is None) or (temp < self.min_temp):
            self.min_temp = temp
        self.total_temps += 1
        self.total_sum += temp

        if temp not in self.freq:
            self.freq[temp] = 1
        else:
            self.freq[temp] += 1

    def get_max(self):
        return self.max_temp

    def get_min(self):
        return self.min_temp

    def get_mean(self):
        self.mean = self.total_sum/(self.total_temps + 0.0)
        return self.mean

    def get_mode(self):
        return self.freq.most_common()[0][0]
