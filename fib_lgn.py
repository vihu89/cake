﻿''' multiply 2x2 matrices

    Fibonacci sequence:
        f(0) = 0
        f(1) = 1
        f(n) = f(n-1) + f(n-2)
        f(n+1) = f(n) + f(n-1)

    v = [f(n), f(n-1)]          # vector
    A*v = w                     # where w = [f(n+1), f(n)]

    z = [f(1), f(0)] = [1, 0]
    Az = [f(2), f(1)]
    AAz = [f(3), f(2)]

    A^(n-1)*z = [f(n), f(n-1)]

'''


def mult(x, y):
    if (len(y) == 2):
        a = x[0]*y[0]+x[1]*y[1]
        b = x[2]*y[0]+x[3]*y[1]
        return [a, b]
    a = x[0]*y[0] + x[1]*y[2]
    b = x[0]*y[1] + x[1]*y[3]
    c = x[2]*y[0] + x[3]*y[2]
    d = x[2]*y[1] + x[3]*y[3]
    return [a, b, c, d]


def exp_by_squaring(x, n):
    if n == 1:
        return x
    elif n == 0:
        return 1
    elif n % 2 == 0:
        return exp_by_squaring(mult(x, x), n/2)
    elif n % 2 != 0:
        return mult(x, exp_by_squaring(mult(x, x), (n-1)/2))


def fastfib(x):
    A = [1, 1, 1, 0]
    v = [1, 0]
    return mult(exp_by_squaring(A, x-1), v)[0]


if __name__ == '__main__':
    x = 10000
    print fastfib(x)
