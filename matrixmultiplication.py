﻿def matrixmult(A, B):
    rows_A = len(A)
    cols_A = len(A[0])
    rows_B = len(B)
    cols_B = len(B[0])

    if cols_A != rows_B:
        return

    # Create the result matrix
    # Dimensions would be rows_A x cols_B
    C = [[0 for row in range(cols_B)] for col in range(rows_A)]

    for i in range(rows_A):
        for j in range(cols_B):
            for k in range(cols_A):
                C[i][j] += A[i][k] * B[k][j]
    return C


x = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
y = [[1, 0, 1], [0, 1, 1], [1, 0, 0]]

xT = [list(i) for i in zip(*x)]
yT = [list(i) for i in zip(*y)]

ans = matrixmult(x, y)
check = [list(i) for i in zip(*matrixmult(yT, xT))]

print ans == check
