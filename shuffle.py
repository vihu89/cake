﻿''' Write a function for doing an in-place shuffle of a list.
    The shuffle must be "uniform," meaning each item in the original list must have the
    same probability of ending up in each spot in the final list.

    Assume that you have a function get_random(floor, ceiling) for getting a random integer
    that is >=floor and <=ceiling.
'''
from random import choice


def shuffle(lst):
    '''
    '''
    pos = 0
    end = len(lst) - 1
    while pos < end:
        pick_item = choice(lst)
        lst.remove(pick_item)
        lst.insert(pos, pick_item)
        pos += 1

    return lst

sample = [10, 20, 30, 40, 50, 60, 70, 30]
print sample
print shuffle(sample)
print sample
