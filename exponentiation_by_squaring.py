﻿''' Reasoning:
    x^8 --> x * x * x * x * x * x * x * x   # Eight times

    # 3 times
    x * x = x^2
    x^2 * x^2 = x^4
    x^4 * x^4 = x^8

    # 5 times
    x^15
    x * x = x^2
    x * x^2 = x^3
    x^3 * x^3 = x^6
    x^6 * x^6 = x^12
    x^3 * x^12 = x^15

    Need a standard way for doing this, instead of tricks

    From wikipedia:

        x^n = { (x^2)^(n/2)             if n is even
                x*((x^2)^(n-1)/2))      if n is odd
              }
'''


def exp(x, n):
    if n < 0:
        return exp(1.0/x, -n)
    elif n == 0:
        return 1
    elif n == 1:
        return x
    elif n % 2 == 0:
        return exp(x*x, n/2)
    elif n % 2 != 0:
        return x * exp(x*x, (n-1)/2)


print exp(4, -5)
print exp(4, 15)
