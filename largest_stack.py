﻿''' Use your Stack class to implement a new class MaxStack with a function get_max() that returns the
    largest element in the stack. get_max() should not remove the item.
    Your stacks will contain only integers.
'''


class Stack(object):

    # initialize an empty list
    def __init__(self):
        self.items = []

    # push a new item to the last index
    def push(self, item):
        self.items.append(item)

    # remove the last item
    def pop(self):

        # if the stack is empty, return None
        # (it would also be reasonable to throw an exception)
        if not self.items:
            return None

        return self.items.pop()

    # see what the last item is
    def peek(self):
        if not self.items:
            return None
        return self.items[len(self.items)-1]

'''
class MaxStack(Stack):
    def __init__(self):
        super(MaxStack, self).__init__()
        self.new_max = 0
        self.old_max = 0

    def push(self, item):
        if item > self.new_max:
            if item > self.old_max:
                self.old_max = self.new_max
            self.new_max = item
        self.items.append(item)

    def pop(self):
        if not self.items:
            return None

        return self.items.pop()

    def get_max(self):
        return self.new_max
'''


class MaxStack(Stack):

    def __init__(self):
        super(MaxStack, self).__init__()
        self.stack = Stack()
        self.maxs_stack = Stack()

    # Add a new item to the top of our stack. If the item is greater
    # than or equal to the the last item in maxs_stack, it's
    # the new max! So we'll add it to maxs_stack.
    def push(self, item):
        self.stack.push(item)
        if item >= self.maxs_stack.peek():
            self.maxs_stack.push(item)

    # Remove and return the top item from our stack. If it equals
    # the top item in maxs_stack, they must have been pushed in together.
    # So we'll pop it out of maxs_stack too.
    def pop(self):
        item = self.stack.pop()
        if (item == self.maxs_stack.peek()):
            self.maxs_stack.pop()
        return item

    # The last item in maxs_stack is the max item in our stack.
    def get_max(self):
        return self.maxs_stack.peek()


if __name__ == '__main__':
    s = MaxStack()
    from random import sample
    lst = sample(xrange(100), 20)

    print 'input list: ', lst
    for i in lst:
        s.push(i)

    print 'items in stack: ', s.items
    print 'max: ', s.get_max()
