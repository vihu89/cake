﻿''' Write a function for finding the index of the "rotation point,"
'''


def find_rotation_index(lst):
    # import pudb; pudb.set_trace()
    if not lst:
        return False
    else:
        # implementation of bonus round, when there is no rotation, it always returns the last index
        # instead just return 0
        index = lst.index(helper(lst))
        if index == (len(lst)-1):
            return 0
        else:
            return index


def helper(lst):
    # base case, element found where rotation is
    if len(lst) == 1:
        return lst[0]
    else:
        # start splitting the list
        mid = len(lst)/2
        # if mid is less than first element
        if lst[mid] < lst[0]:
            # and if mid is greater than the last element in the first half of the list
            if lst[mid] > lst[:mid][-1]:
                # go left
                return helper(lst[:mid])
            # otherwise if mid is less than the last element in the first half of the list
            elif lst[mid] <= lst[:mid][-1]:
                # go right
                return helper(lst[mid:])
        # if mid is greater than first element
        if lst[mid] > lst[0]:
            if lst[mid] > lst[mid:][-1]:
                return helper(lst[mid:])
            elif lst[mid] <= lst[mid:][-1]:
                return helper(lst[:mid])


if __name__ == '__main__':
    words = ['k', 'l', 'v', 'a', 'b', 'c', 'd', 'e', 'g', 'i']
    words2 = [
        'ptolemaic',
        'quiver',
        'retrograde',
        'supplant',
        'undulate',
        'xenoepist',
        'asymptote',    # <-- rotates here!
        'babka',
        'banoffee',
        'engender',
        'karpatka',
        'othellolagkage',
    ]
    numbers = [90, 80, 70, 60, 50, 10, 20, 30, 40]
    numbers2 = [90, 80, 70, 60, 50]
    print find_rotation_index(words)
    print find_rotation_index(words2)
    print find_rotation_index(numbers)
    print find_rotation_index(numbers2)
