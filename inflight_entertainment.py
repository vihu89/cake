﻿''' Users on longer flights like to start a second movie right when their first one ends,
    but they complain that the plane usually lands before they can see the ending.
    So you're building a feature for choosing two movies whose total runtimes will equal
    the exact flight length.

    Write a function that takes an integer flight_length (in minutes) and a
    list of integers movie_lengths (in minutes) and returns a boolean indicating whether
    there are two numbers in movie_lengths whose sum equals flight_length.

    When building your function:

    Assume your users will watch exactly two movies
    Don't make your users watch the same movie twice
    Optimize for runtime over memory
'''


def movie_recommendation(flight_length, movie_lengths):
    ''' flight_lenght: integer
        movie_lenghts: list of integers
        Returns: Bool True/False, indicating, if there are two numbers in movie_lenghts whose sum = flight_length
    '''
    movie_lengths_seen = set()
    for first_movie_length in movie_lengths:
        second_movie_length = flight_length - first_movie_length
        print first_movie_length, second_movie_length, movie_lengths_seen
        if second_movie_length in movie_lengths_seen:
            return True

        movie_lengths_seen.add(first_movie_length)
    return False


if __name__ == '__main__':
    lst = [60, 120, 180, 60, 100, 180, 140, 90, 90, 120, 120]
    print sum(lst)
    print movie_recommendation(360, lst)
