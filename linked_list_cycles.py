﻿''' Write a function contains_cycle() that takes the first node in a singly-linked list and
    returns a boolean indicating whether the list contains a cycle.

    A cycle occurs when a node’s next points back to a previous node in the list.
    The linked list is no longer linear with a beginning and end—instead, it cycles through a loop of nodes.
'''


class LinkedListNode:

    def __init__(self, value):
        self.value = value
        self.next = None


def contains_cycle(head):
    ''' head is the first node in the linked list
        return: Bool True if there is a cycle in the linked list, False otherwise
    '''
    current = head
    seen_nodes = {current}
    cycle = False
    while not cycle:
        if current.next:
            if current.next not in seen_nodes:
                seen_nodes.add(current.next)
                current = current.next
            else:
                cycle = True
        else:
            break

    return cycle


a = LinkedListNode('A')
b = LinkedListNode('B')
a2 = LinkedListNode('A')
c = LinkedListNode('C')
d = LinkedListNode('D')

a.next = b
b.next = a2
a2.next = c
c.next = d
d.next = a

print contains_cycle(a)
