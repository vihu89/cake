﻿''' You created a game that is more popular than Angry Birds.
    You rank players in the game from highest to lowest score. So far you're using an algorithm that sorts in
    O(nlgn) time, but players are complaining that their rankings aren't updated fast enough. You need a faster
    sorting algorithm.

    Write a function that takes:
        a list of unsorted_scores
        the highest_possible_score in the game
        and returns a sorted list of scores in less than O(nlgn) time.
'''


def sort_scores(unordered_scores, highest_possible_score):

    # list of 0s at indices 0..highest_possible_score
    scores_to_counts = [0] * (highest_possible_score+1)
    print scores_to_counts

    # populate scores_to_counts
    for score in unordered_scores:
        scores_to_counts[score] += 1

    print scores_to_counts

    # populate the final sorted list
    sorted_scores = []

    # for each item in scores_to_counts
    for score, count in enumerate(scores_to_counts):

        # for the number of times the item occurs
        for time in range(count):

            # add it to the sorted list
            sorted_scores.append(score)
            print sorted_scores

    return sorted_scores


print sort_scores([10, 30, 20, 20, 10, 30, 40, 50, 40, 30, 20, 30, 10, 60, 30], 70)
