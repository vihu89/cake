﻿''' Write a function for reversing a linked list. Do it in-place.
    Your function will have one input: the head of the list.
    Your function should return the new head of the list.
'''


class LinkedListNode:

    def __init__(self, value):
        self.value = value
        self.next = None


def reverse_linked_list_in_place(head):
    ''' return new head of list after reversing in place
    '''
    current = head
    node_stack = [current]
    while current.next:
        node_stack.append(current.next)
        current = current.next

    head = node_stack.pop()
    current = head
    while node_stack:
        current.next = node_stack.pop()
        current = current.next

    current.next = None

    return head


def reverse(head_of_list):
    current = head_of_list
    previous = None
    next = None

    # until we have 'fallen off' the end of the list
    while current:

        # copy a pointer to the next element
        # before we overwrite current.next
        next = current.next

        # reverse the 'next' pointer
        current.next = previous

        # step forward in the list
        previous = current
        current = next

    return previous


a = LinkedListNode('A')
b = LinkedListNode('B')
c = LinkedListNode('C')
d = LinkedListNode('D')

a.next = b
b.next = c
c.next = d

print reverse_linked_list_in_place(a)
print reverse(a)
