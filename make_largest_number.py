﻿''' Given digits make largest number from them
'''


def make_largest(given):
    '''
    '''
    num_list = list(str(given))
    new_list = []
    while num_list:
        max_so_far = 0
        for i in num_list:
            if i > max_so_far:
                max_so_far = i
        new_list.append(max_so_far)
        num_list.remove(max_so_far)

    return int(''.join(new_list))


sample = 98526792374
print make_largest(sample)
