import itertools

# TEST DATERS
movie_lengths = [2, 2, 2, 1, 2, 2, 3, 2, 4,]
flight_lenght = 4
###


SEEN_DB = set()

def seen(pair):
    if pair in SEEN_DB:
        return True
    else:
        SEEN_DB.add(pair)
        return False

def get_goods(movie_lengths, flight_length):
    for pair in itertools.combinations(movie_lengths, 2):
        if not seen(pair) and sum(pair) == flight_lenght:
            yield pair

for x in get_goods(movie_lengths, flight_lenght):
    print x
