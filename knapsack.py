﻿from random import random


def build_items(n):
    res = []
    for i in xrange(n):
        res.append((i, 1+int(9*random()), 1+int(9*random())))
    return res


def powerset(items):
    res = [[]]
    for item in items:
        newset = [r+[item] for r in res]
        res.extend(newset)
    return res


def knapsack_brute_force(items, max_weight):
    knapsack = []
    best_weight = 0
    best_value = 0
    for item_set in powerset(items):
        set_weight = sum([e[1] for e in item_set])
        set_value = sum([e[2] for e in item_set])
        if set_value > best_value and set_weight <= max_weight:
            best_value = set_value
            best_weight = set_weight
            knapsack = item_set
    return knapsack, best_weight, best_value


def weight(item):
    return item[1]


def value(item):
    return item[2]


def density(item):
    return float(item[2]/item[1])


def knapsack_greedy(items, max_weight, keyFunc=value):
    knapsack = []
    knapsack_weight = 0
    knapsack_value = 0
    items_sorted = sorted(items, key=keyFunc)
    while len(items_sorted) > 0:
        item = items_sorted.pop()
        if weight(item) + knapsack_weight <= max_weight:
            knapsack.append(item)
            knapsack_weight += weight(item)
            knapsack_value += value(item)
        else:
            break
    return knapsack, knapsack_weight, knapsack_value


data = build_items(25)
print 'data', data
pset = powerset(data)

max_w = 10

kn, opt_w, opt_v = knapsack_greedy(data, max_w, weight)
print 'knapsack',  kn
kn, opt_w, opt_v = knapsack_greedy(data, max_w, value)
print 'knapsack',  kn
kn, opt_w, opt_v = knapsack_greedy(data, max_w, density)
print 'knapsack',  kn
