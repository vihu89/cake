﻿''' Write a function max_duffel_bag_value() that takes a list of cake type tuples and a weight capacity,
    and returns the maximum monetary value the duffel bag can hold.
'''


def max_duffel_bag_value_with_capacity_1(cake_tuples):

    max_value_at_capacity_1 = 0

    for cake_weight, cake_value in cake_tuples:
        if (cake_weight == 1):
            max_value_at_capacity_1 = max(max_value_at_capacity_1, cake_value)

    return max_value_at_capacity_1


cake_tuples = [(7, 160), (3, 90), (2, 15)]
capacity = 20
# returns 555 (6 of the middle type of cake and 1 of the last type of cake)
ans = 555

sample_cakes = [(1, 30), (50, 200)]
sample_cap = 100
sample_ans = 3000
