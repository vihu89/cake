﻿''' merging ranges
'''
import unittest


given = [(0, 1), (3, 5), (4, 8), (10, 12), (9, 10)]
result = [(0, 1), (3, 8), (9, 12)]

simple = [(1, 3), (2, 4)]
ans = [(1, 4)]


def condense_meeting_times(lst):
    # sort the meetings based on their starting time
    sorted_meeting_list = sorted(lst, key=lambda meet: meet[0])
    # first meeting will always be at the beginning of the sorted list
    merged_meetings = [sorted_meeting_list[0]]
    # from the second meeting onwards start merging the meetings

    for current_meeting_start, current_meeting_end in sorted_meeting_list[1:]:
        last_merged_meeting_start, last_merged_meeting_end = merged_meetings[-1]
        if current_meeting_start <= last_merged_meeting_end:
            merged_meetings[-1] = (last_merged_meeting_start, max(last_merged_meeting_end, current_meeting_end))
        else:
            merged_meetings.append((current_meeting_start, current_meeting_end))


    return merged_meetings


class TestSimple(unittest.TestCase):
    def testEqual(self):
        self.failUnlessEqual(condense_meeting_times(simple), ans)


class TestFinal(unittest.TestCase):
    def testEqual(self):
        self.failUnlessEqual(condense_meeting_times(given), result)


if __name__ == '__main__':
    test_classes_to_run = [TestSimple, TestFinal]

    loader = unittest.TestLoader()

    suites_list = []
    for test_class in test_classes_to_run:
        suite = loader.loadTestsFromTestCase(test_class)
        suites_list.append(suite)

    big_suite = unittest.TestSuite(suites_list)

    runner = unittest.TextTestRunner()
    results = runner.run(big_suite)
