﻿''' Delete a node from a singly-linked list , given only a variable pointing to that node.
    Caveat: Can't delete the last node using this method
            Dangling node left which is inaccessible from the list
'''


class LinkedListNode:

    def __init__(self, value):
        self.value = value
        self.next = None


def delete_node(node):
    print 'before\n'
    print node
    print node.value
    print node.next

    node.value = node.next.value
    node.next = node.next.next

    print 'after\n'
    print node
    print node.value


a = LinkedListNode('A')
b = LinkedListNode('B')
c = LinkedListNode('C')

a.next = b
b.next = c

delete_node(b)
