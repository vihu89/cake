﻿''' "Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing."

    Write a function that, given a sentence like the one above, along with the position of an
    opening parenthesis, finds the corresponding closing parenthesis.

    Example: if the example string above is input with the number 10
    (position of the first parenthesis), the output should be 79 (position of the last parenthesis).
'''


def get_closing_paren(string, opening_index):
    '''
    '''
    pos = opening_index
    count_additional_openers = 0
    while pos < len(string):
        if string[pos] == '(':
            count_additional_openers += 1
        if string[pos] == ')':
            count_additional_openers -= 1
        if count_additional_openers == 0:
            return pos
        pos += 1


if __name__ == '__main__':
    test = "Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing."

    test2 = 'ra(hul(ga(rg)))'
    y = get_closing_paren(test, 10)
    z = get_closing_paren(test2, 6)
    print y
    print z
