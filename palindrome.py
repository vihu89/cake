﻿''' Check palindrom recursively
'''


def isPalindromeRecursive(string):
    if len(string) in [0, 1]:
        return True
    if string[0] == string[-1]:
        return isPalindromeRecursive(string[1:len(string)-1])
    return False


def isPalindromeIter(string):
    left = 0
    right = len(string) - 1
    valid = True

    while left < right:

        if string[left] != string[right]:
            valid = False
            break

        left += 1
        right -= 1

    return valid
