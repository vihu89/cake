﻿''' I have a list where every number in the range 1..n appears once except for one number
    which appears twice.
    Write a function for finding the number that appears twice.
'''


def which_appears_twice(lst):
    ''' with O(n) space
    '''
    seen = set()
    for i in lst:
        if i not in seen:
            seen.add(i)
        else:
            return i


def which_appears_twice_math(lst):
    ''' In O(1) space
    '''
    n = lst[-1]
    expected_sum = n * (n+1)/2
    actual_sum = sum(lst)
    return actual_sum - expected_sum


print which_appears_twice([1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10]) == 5
print which_appears_twice_math([1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10]) == 5
