﻿''' understanding trees
'''
from random import sample


class Node(object):
    def __init__(self, data=None):
        self.data = data
        self.left = None
        self.right = None
        self.parent = None


class BST(object):
    def __init__(self, root=None):
        self.root = root
        self.total_nodes = 0

    def add(self, value):
        if self.root is None:
            self.total_nodes += 1
            self.root = Node(value)
            self.root.parent = None
        else:
            self._add(value, self.root)

    def _add(self, value, node):
        if value < node.data:
            if node.left is None:
                self.total_nodes += 1
                node.left = Node(value)
                node.left.parent = node
            else:
                self._add(value, node.left)
        else:
            if node.right is None:
                self.total_nodes += 1
                node.right = Node(value)
                node.right.parent = node
            else:
                self._add(value, node.right)

    def find(self, value):
        if self.root is None:
            return False
        else:
            return self._find(value, self.root)

    def _find(self, value, node):
        if value == node.data:
            return node
        elif value < node.data and node.left is not None:
            return self._find(value, node.left)
        elif value > node.data and node.right is not None:
            return self._find(value, node.right)

    @property
    def lowest(self):
        if self.root is None:
            return False
        else:
            return self._lowest(self.root)

    def _lowest(self, node):
        if node.left:
            return self._lowest(node.left)
        return node.data

    @property
    def largest(self):
        if self.root is None:
            return False
        else:
            return self._largest(self.root)

    def _largest(self, node):
        if node.right:
            return self._largest(node.right)
        return node

    @property
    def second_largest(self):
        largest = self.largest
        if largest.left is None:
            return largest.parent
        else:
            return self._largest(largest.left)

    @property
    def inorder(self):
        if self.root is None:
            return False
        else:
            return self._inorder(self.root)

    def _inorder(self, node):
        if node.left:
            self._inorder(node.left)
        print node.data
        if node.right:
            self._inorder(node.right)

    @property
    def preorder(self):
        if self.root is None:
            return False
        else:
            return self._preorder(self.root)

    def _preorder(self, node):
        print node.data
        if node.left:
            self._preorder(node.left)
        if node.right:
            self._preorder(node.right)

    @property
    def postorder(self):
        if self.root is None:
            return False
        else:
            return self._postorder(self.root)

    def _postorder(self, node):
        if node.left:
            self._postorder(node.left)
        if node.right:
            self._postorder(node.right)
        print node.data

    @property
    def iter_preorder(self):
        if self.root is None:
            return False
        else:
            return self._iter_preorder(self.root)

    def _iter_preorder(self, node):
        stack = []
        stack.append(node)
        while stack:
            current = stack.pop()
            print current.data
            if current.right:
                stack.append(current.right)
            if current.left:
                stack.append(current.left)

    def iter_inorder(self):
        current = self.root
        stack = []
        done = False

        while not done:
            if current:
                stack.append(current)
                current = current.left
            else:
                if stack:
                    current = stack.pop()
                    print current.data
                    current = current.right
                else:
                    done = True


def test():
    tree = BST()
    tree.add(3)
    tree.add(4)
    tree.add(0)
    tree.add(8)
    tree.add(-5)
    tree.add(2)
    tree.add(7)
    print tree.find(4).data
    print tree.find(10)
    print tree.largest.data
    print tree.lowest
    print tree.second_largest.data
    return tree


def test2():
    tree = BST()

    lst = sample(range(-100, 100), 32)
    print 'sample: ', lst

    for i in lst:
        tree.add(i)

    assert len(lst) == tree.total_nodes

    print tree.largest.data
    print tree.lowest
    print tree.second_largest.data

    return tree


if __name__ == '__main__':
    bst = test()
    bst2 = test2()
