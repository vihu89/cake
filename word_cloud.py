﻿''' Build word cloud in a dictionary
'''


def build_word_cloud(string):
    cloud = {}
    for i in string.lower().split():
        if i in cloud:
            cloud[i] += 1
        else:
            cloud[i] = 1
    return cloud

print build_word_cloud('Add milk and eggs, then add flour and sugar.')
