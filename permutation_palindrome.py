﻿''' Write an efficient function that checks whether any permutation of an input string is a palindrome.

    Examples:
    "civic" should return True
    "ivicc" should return True
    "civil" should return False
    "livci" should return False
'''

def permutation_palindrome(string):
    unpaired = set()
    for char in string:
        if char in unpaired:
            unpaired.remove(char)
        else:
            unpaired.add(char)
    return len(unpaired) <= 1


print permutation_palindrome('civic')
print permutation_palindrome('ivicc')
print permutation_palindrome('civil')
print permutation_palindrome('livic')
