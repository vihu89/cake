﻿''' Implement a queue with 2 stacks. Your queue should have an enqueue and a dequeue function and
    it should be "first in first out" (FIFO).
    Optimize for the time cost of mm function calls on your queue.
    These can be any mix of enqueue and dequeue calls.
    Assume you already have a stack implementation and it gives O(1) time push and pop.
'''


class Queue(object):
    def __init__(self):
        self.head = []
        self.tail = []

    def enqueue(self, item):
        self.head.append(item)
        self.tail.insert(0, self.head.pop())

    def dequeue(self):
        return self.tail.pop()


if __name__ == '__main__':
    q = Queue()
    q.enqueue('a')
    q.enqueue('b')
    q.enqueue('c')
    q.enqueue('d')
    assert 'a' == q.dequeue()
    assert 'b' == q.dequeue()
    assert 'c' == q.dequeue()
    q.enqueue('e')
    assert 'd' == q.dequeue()
    assert 'e' == q.dequeue()
