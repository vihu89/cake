''' Write a function to reverse a string in-place.

    Since strings in Python are immutable , first convert the string into a list of characters,
    do the in-place reversal on that list, and re-join that list into a string before returning it.
    This isn't technically "in-place" and the list of characters will cost O(n) additional space,
    but it's a reasonable way to stay within the spirit of the challenge.
    If you're comfortable coding in a language with mutable strings, that'd be even better!
'''


class LinkedListNode:
    def __init__(self, value):
        self.value = value
        self.next = None


def reverse(head_of_list):
    current = head_of_list
    previous = None
    next = None

    # until we have 'fallen off' the end of the list
    while current:

        # copy a pointer to the next element
        # before we overwrite current.next
        next = current.next

        # reverse the 'next' pointer
        current.next = previous

        # step forward in the list
        previous = current
        current = next

    return previous


def reverse_ans(string):
    string_list = list(string)

    left_pointer = 0
    right_pointer = len(string_list) - 1

    while left_pointer < right_pointer:

        # swap characters
        string_list[left_pointer], string_list[right_pointer] = \
            string_list[right_pointer], string_list[left_pointer]

        # move towards middle
        left_pointer += 1
        right_pointer -= 1

    return ''.join(string_list)


x = 'rahulgarg2'
print reverse_ans(x)
